/**
 * Created by Owner on 6/16/2017.
 */

// done: Grab the user designated free time hours
  // done: convert the hours into a start and end. each pair gets converted to an object (start, end) and pushed into an array.
  // acv: each push should be destructive to old values
// acv: Each object has 2 keys: start, end.

// todo: Set up an iterator to check each range for an in-between





var free_time_hours = document.getElementById('ftid')
  free_time_hours.addEventListener("blur", save_free_time_hours)





// this function is written ahead of save_free_time_hours
function hour_pair (start, end) {
  this.start = start
  this.end = end
}




let split_objects = []

function save_free_time_hours () {

  // done: break the new lines into array elements. Reset the array each time

  var splitting_array = free_time_hours.value.split(/\n/)

  // acv: use the map function to map each string to an object

  // console.log("splitting_array")
  // console.log(splitting_array)

  split_objects = splitting_array.map(function(str) {
    var box = str.split('-') // box[0] = 1200, box[1] = 0100

    // convert all strings in the array to integers
    for (var i = 0; i < box.length; i++) {
      box[i] = parseInt(box[i])
    }

    // it's the map method that's looping these console logs
    // I'm still not holding in mind the fact that everything is being iterated

    // console.log(box[0])
    // console.log(box)

    return {
      start: box[0],
      end: box[1]
      }

  })
  // console.log("objectified start/end values")
  // console.log(split_objects)
  //this is the correct place for console logs. the function fires on blur. the global scope does not

}



// done: Capture the user's current time

let normalized_time

function get_current_time () {
  var js_datetime = new Date()

// done: Extract the time. Normalize it into HHMM

  var js_date = new Date()

  let current_hour = js_date.getHours()

  if (current_hour < 10) {
    current_hour = "" + 0 + js_date.getHours()
  }

  let current_minute = js_date.getMinutes()

  if (current_minute < 10) {
    current_minute = "" + 0 + current_minute
  }

  normalized_time = "" + current_hour + current_minute

  normalized_time = parseInt(normalized_time)

  return normalized_time

}





// todo: create a function that checks if the current time is in the free time range (or not)
// todo: test this once I have the website checker working

function free_time_check () {
  //check each object range for a match (above start, below end)
  //if match return true. else return false

    for (var i = 0; i < split_objects.length; i++) {
      var obj = split_objects[i]; // remember to access obj.start and .end

      if (normalized_time >= obj.start && normalized_time <= obj.end) {
        return true
      } else {
        return false
      }
    }

    // This will now connect to a routine that sends the user to the note page if false
    // Be very careful when testing this. Ensure I have a way to shut off the plugin

  }








// Setup a URL watcher. Setup a time checker. Setup a block redirect subroutine.
/*
* IF index of *browser_URL* >0 AND timeStamp ===
* */
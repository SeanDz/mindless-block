/**
 * Created by Owner on 6/16/2017.
 */

console.log("note.js connected");

///// select the text area

var note_area = document.getElementById('notetext');

//// todo Create a function to save the textarea to local storage
// todo First check & load any previously saved notes

(function load_notes() {
  if (note_area === "") {
    chrome.storage.sync.get("stored_obj", function(resultObj) {
      note_area.value = resultObj.stored_obj;
      console.log("Attempted to load notes into a blank note area. Did it run correctly?");
    });
  }
})();

function save_notes () {

  chrome.storage.sync.set({
    stored_obj: note_area.value
  }, function () {
    // console.log("Saved into Chrome Storage")
  });

  // chrome.privacy.services.autofillEnabled.get({}, function(details) {
  //   if (details.value)
  //     console.log('Autofill is on!');
  //   else
  //     console.log('Autofill is off!');
  // });

}

note_area.addEventListener('blur', save_notes);

//// setup a blur handler in notes.html to fire function save_notes
// could not be done (inline JS). I need to add a listener and a function in this file

function console_fire() {
  console.log ('fired the console log')
}




//   })
// } else {
//   console.log ("Either notetext is null or some issue has happened")
// }

// document.querySelector('#notetext').addEventListener('blur', function () {
//   console.log("focus lost")
// })

